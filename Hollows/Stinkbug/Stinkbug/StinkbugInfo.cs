﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Stinkbug
{
    public class StinkbugInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Stinkbug";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                //Return a 24x24 pixel bitmap to represent this GHA library.
                return null;
               // return Stinkbug.Properties.Resources.Icon;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("91de9507-09e6-4c05-8e16-37367ac83b6e");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "";
            }
        }
    }
}
