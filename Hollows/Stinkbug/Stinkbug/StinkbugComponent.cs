﻿using Rhino;
using Rhino.Geometry;
using Rhino.DocObjects;
using Rhino.Collections;

using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;

using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;


namespace Stinkbug
{
    public class StinkbugComponent : GH_Component
    {
        /// <summary>
        /// Each implementation of GH_Component must provide a public 
        /// constructor without any arguments.
        /// Category represents the Tab in which the component will appear, 
        /// Subcategory the panel. If you use non-existing tab or panel names, 
        /// new tabs/panels will automatically be created.
        /// </summary>
        public StinkbugComponent()
          : base("Stinkbug", "Stinkbug",
              "Process voxel blocks for habit hollows.",
              "Deep Design Lab", "Hollows")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            // Use the pManager object to register your input parameters.
            // You can often supply default values when creating parameters.
            // All parameters must have the correct access type. If you want 
            // to import lists or trees of values, modify the ParamAccess flag.

            pManager.AddPointParameter("centroids", "p", "A list of voxel centroids", GH_ParamAccess.list);
            pManager.AddNumberParameter("spacing", "s", "spacing of voxel grid", GH_ParamAccess.item, 1.0);
            pManager.AddIntegerParameter("testIndex", "i", "extract a voxel", GH_ParamAccess.item);


            // If you want to change properties of certain parameters, 
            // you can use the pManager instance to access them by index:
            //pManager[0].Optional = true;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // Use the pManager object to register your output parameters.
            // Output parameters do not have default values, but they too must have the correct access type.

            //pManager.AddTextParameter("test", "test", "test", GH_ParamAccess.item);
            
            pManager.AddPointParameter("OutVoxelCentroids", "O", "out voxels", GH_ParamAccess.list);
            pManager.AddPointParameter("thisVoxel", "Vi", "thisVoxel", GH_ParamAccess.item);
            pManager.AddPointParameter("thisVoxelNeighbours", "Ni", "Neighbours", GH_ParamAccess.list);
            pManager.AddPointParameter("testedLocations", "Ti", "testedLocations", GH_ParamAccess.list);
            pManager.AddPointParameter("Voxels and their neighbours in Tree", "Xi", "X", GH_ParamAccess.tree);

            pManager.AddIntegerParameter("test", "t", "t", GH_ParamAccess.list);

            pManager.AddPointParameter("Voxels in clusters", "C", "C", GH_ParamAccess.tree);
            // Sometimes you want to hide a specific parameter from the Rhino preview.
            // You can use the HideParameter() method as a quick way:
            //pManager.HideParameter(0);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object can be used to retrieve data from input parameters and 
        /// to store data in output parameters.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // First, we need to retrieve all data from the input parameters.
            // We'll start by declaring variables and assigning them starting values.

            List<Point3d> inCentroids = new List<Point3d>();// { Point3d.Origin };
            double spacing = 1.0;
            int thisVoxelindex = 0;
          

            // Then we need to access the input parameters individually. 
            // When data cannot be extracted from a parameter, we should abort this method.

            if (!DA.GetDataList(0, inCentroids)) return;
            if (!DA.GetData(1, ref spacing)) return;
            if (!DA.GetData(2, ref thisVoxelindex)) return;

            // We should now validate the data and warn the user if invalid data is supplied.

            if (inCentroids.Count <= 1) //0?
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Less than one voxel is registered");
                return;
            }
            // We're set to create the solver now. To keep the size of the SolveInstance() method small, 
            // The actual functionality will be in a different method:

            List<VoxelClass> outVoxels = ProcessVoxels(inCentroids, spacing);

            List<Point3d> voxelCentroids = GetCentroids(outVoxels);

            ClusterVoxels(outVoxels);

            foreach (VoxelClass voxel in outVoxels)
            {
                //MineNeighbours(voxel, voxel);
            }

            VoxelClass thisVoxel = outVoxels[thisVoxelindex];
                
            List<VoxelClass> thisVoxelNeighbours = GetThisVoxelNeighbours(thisVoxel);

            Point3d thisLoc = thisVoxel.centroid;

            List<Point3d> testedPoints = new List<Point3d>(thisVoxel.testedNeighbourcentroids);


            DataTree<Point3d> voxelsInATree = GetVoxelsInTree(outVoxels, 1);

            DataTree<Point3d> clustersInATree = GetClustersInTree(outVoxels, 1);


            //////////////////////////

            List<bool> hasVoxelNeighbours = GetBool(outVoxels);

            List<VoxelClass> voxelsAndNeighbours = GetNeighboursInOrder(outVoxels);

            List<int> parentIndexes = GetParentIndexes(voxelsAndNeighbours);

            List <TestRecursive> objects = new List<TestRecursive>();

            for (int i = 0; i < 10; i++)
            {
                objects.Add(new TestRecursive(i));
            }

            for (int i = 0; i < objects.Count; i++)
            {
                if (i + 2 < objects.Count)
                {
                    objects[i].neighbous.Add(objects[i + 1]);
                }
            }

            //GetTestRecursive(objects[0], objects[0]);

            //////

            List<int> testRecursiveNeighbourInts = new List<int>();

            foreach (TestRecursive neighbour in objects[0].neighbous)
            {
               testRecursiveNeighbourInts.Add(neighbour.id);
            }

            // Finally assign the processed data to the output parameter

            DA.SetDataList(0, GetCentroids(outVoxels));
            DA.SetData(1, GetCentroids(thisVoxel));
            DA.SetDataList(2, GetCentroids(thisVoxelNeighbours));
            DA.SetDataList(3, testedPoints);
            DA.SetDataTree(4, voxelsInATree);
            DA.SetDataList(5, testRecursiveNeighbourInts);
            DA.SetDataTree(6, clustersInATree);
        }

        

        void GetTestRecursive (TestRecursive origRec, TestRecursive neighbourRec)
        {
            if (origRec.neighbous.Count < 1)
            {
                return;
            }

            else foreach (TestRecursive _neighbourRec in neighbourRec.neighbous)
            {
                origRec.neighbous.Add(_neighbourRec);
                GetTestRecursive(origRec, _neighbourRec);
            }
        }

        DataTree<VoxelClass> GetVoxelsInTree(List<VoxelClass> allVox)
        {
            DataTree<VoxelClass> _allVox = new DataTree<VoxelClass>();

            for (int i = 0; i < allVox.Count; i++)
            {
                for (int j = 0; j < allVox[i].neighbours.Count; j++)
                {
                    GH_Path pth = new GH_Path(i);
                    _allVox.Add(allVox[i].neighbours[j], pth);
                }
            }

            return _allVox;
        }

        DataTree<Point3d> GetVoxelsInTree(List<VoxelClass> allVox, int hello)
        {
            DataTree<Point3d> _allVox = new DataTree<Point3d>();

            for (int i = 0; i < allVox.Count; i++)
            {
                GH_Path pth = new GH_Path(i);

                _allVox.Add(allVox[i].centroid, pth);

                for (int j = 0; j < allVox[i].neighbours.Count; j++)
                {
                    _allVox.Add(allVox[i].neighbours[j].centroid, pth);
                }
            }

            return _allVox;
        }

        DataTree<VoxelClass> GetClustersInTree(List<VoxelClass> allVox)
        {
            DataTree<VoxelClass> _allVox = new DataTree<VoxelClass>();

            foreach (VoxelClass voxel in allVox)
            {
                GH_Path path = new GH_Path(voxel.clusterID);
                _allVox.Add(voxel, path);
            }

            return _allVox;
        }

        DataTree<Point3d> GetClustersInTree(List<VoxelClass> allVox, int hello)
        {
            DataTree<Point3d> _allVox = new DataTree<Point3d>();

            foreach (VoxelClass voxel in allVox)
            {
                GH_Path path = new GH_Path(voxel.clusterID);
                _allVox.Add(voxel.centroid, path);
            }

            return _allVox;
        }

        List<Point3d> TestVoxels(List<Point3d> inVox)
        {
            List<Point3d> outVox = new List<Point3d>(inVox);
            return
                outVox;
        }

        List<VoxelClass> ProcessVoxels(List<Point3d> centroids, double spacing)
        {

            List<VoxelClass> outVox = new List<VoxelClass>();

            foreach (Point3d centroid in centroids)
            {
                outVox.Add(new VoxelClass(centroid));
            }

            outVox = FindNeighbours(outVox, spacing);
            

            return
                outVox;
        }

        List<Point3d> GetCentroids(List<VoxelClass> voxels)
        {
            List<Point3d> centroids = new List<Point3d>();
            foreach (VoxelClass voxel in voxels)
            {
                centroids.Add(voxel.centroid);
            }

            return
                centroids;
        }

        Point3d GetCentroids(VoxelClass voxel)
        {

            Point3d centroid  = new Point3d(voxel.centroid);

            return
                centroid;
        }

        List<bool> GetBool(List<VoxelClass> voxels)
        {
            List<bool> boolNeighbours = new List<bool>();
            foreach (VoxelClass voxel in voxels)
            {
                if (voxel.hasNeighbours == true)
                {
                    boolNeighbours.Add(voxel.hasNeighbours);
                }
            }

            return
                boolNeighbours;
        }

        void MineNeighbours(VoxelClass minedVoxel, VoxelClass origVoxel)
        {
            if (minedVoxel.neighbours.Count < 1)
            {
                return;
            }

            else foreach (VoxelClass minedNeighbour in minedVoxel.neighbours)
            {
                    origVoxel.neighbours.Add(minedNeighbour);
                    MineNeighbours(minedNeighbour, origVoxel);
            }
        }

        void ClusterVoxels(List<VoxelClass> allVoxels)
        {
            foreach (VoxelClass thisVoxel in allVoxels)
            {
                //create new clusterID
                if (thisVoxel.clusterID == -1)
                {
                    thisVoxel.clusterID = VoxelClass.idCounter;
                    VoxelClass.idCounter++;
                }

                //check thisVoxel's neighbours
                foreach (VoxelClass neighbourVox in thisVoxel.neighbours)
                {
                    //change neighbourVoxel's clusterID to this voxel's clusterID
                    neighbourVox.clusterID = thisVoxel.clusterID;

                    //add neighbour voxels to this voxel's cluster if they aren't already added
                    if (!thisVoxel.cluster.Contains(neighbourVox))
                    {
                        thisVoxel.cluster.Add(neighbourVox);
                    }

                    //add all voxels in cluster to the cluster of neighbouring voxels
                    //change their clusterIDs to thisVoxel's clusterID

                    foreach (VoxelClass clusterVox in thisVoxel.cluster)
                    {
                        if (!thisVoxel.cluster.Contains(neighbourVox))
                        {
                            neighbourVox.cluster.Add(clusterVox);
                        }
                        clusterVox.clusterID = thisVoxel.clusterID;
                    }
                }
            }
        }

        //not used
        void FindNeighbours2(List<VoxelClass> allVoxels, double spacing, ref List<VoxelClass> processedVoxels, ref List<Point3d> neighbourOriginsTest)
        {
            

            for (int i = 0; i < allVoxels.Count; i++)
            {


                Vector3d locA = new Vector3d(allVoxels[i].centroid);

                List<Vector3d> locA2 = new List<Vector3d>()
                   {
                       new Vector3d(locA.X + spacing, locA.Y, locA.Z),
                       new Vector3d(locA.X - spacing, locA.Y, locA.Z),
                       new Vector3d(locA.X, locA.Y, locA.Z + spacing),
                       new Vector3d(locA.X, locA.Y, locA.Z - spacing)
                   };

                for (int j = 0; j < allVoxels.Count; j++)
                {

                    Vector3d locB = new Vector3d(allVoxels[j].centroid);

                    bool isBreak = false;

                    for (int k = 0; k < locA2.Count; k++)
                    {
                        allVoxels[i].testedNeighbourcentroids.Add(new Point3d(locA2[k]));

                        if (locB == locA2[k])
                        {
       
                            allVoxels[i].neighbours.Add(allVoxels[j]);

                        }
                    }

                    // if (isBreak) break;
                }


            }

            processedVoxels = new List<VoxelClass>(allVoxels);

        }


        List<VoxelClass> FindNeighbours(List<VoxelClass> allVoxels, double spacing)
        {      
            for (int i = 0; i < allVoxels.Count; i++)
            {
               
                
               Vector3d locA = new Vector3d(allVoxels[i].centroid);

               List<Vector3d> locA2 = new List<Vector3d>()
                   {
                       new Vector3d(locA.X + spacing, locA.Y, locA.Z),
                       new Vector3d(locA.X - spacing, locA.Y, locA.Z),
                       new Vector3d(locA.X, locA.Y, locA.Z + spacing),
                       new Vector3d(locA.X, locA.Y, locA.Z - spacing)
                   };

               for (int j = 0; j < allVoxels.Count; j++)
               {

                   Vector3d locB = new Vector3d(allVoxels[j].centroid);

                   bool isBreak = false;

                   for (int k = 0; k < locA2.Count; k++)
                   {
                        allVoxels[i].testedNeighbourcentroids.Add(new Point3d(locA2[k]));

                        if (locB == locA2[k])
                       {
                            /*allVoxels[i].neighbours.Add(allVoxels[j]);
                            allVoxels[i].parentIndexes.Add(i);
                            allVoxels[i].hasNeighbours = true;
                            isBreak = true;
                             */

                          //  allVoxels[i].neighbours.Add(new VoxelClass(allVoxels[j].centroid));

                            allVoxels[i].neighbours.Add(allVoxels[j]);

                        }
                   }

                  // if (isBreak) break;
               }
               

            }

            return allVoxels;
        }

        List<VoxelClass> GetNeighboursInOrder(List<VoxelClass> allVoxels)
        {
            List<VoxelClass> voxelsandNeighbours = new List<VoxelClass>();
   
            foreach (VoxelClass voxel in allVoxels)
            {
                foreach (VoxelClass neighbour in voxel.neighbours)
                {
                    voxelsandNeighbours.Add(neighbour);
                }
            }
            return
                voxelsandNeighbours;
        }

        List<int> GetParentIndexes (List<VoxelClass> allVoxels)
        {
            List<int> parentIndex = new List<int>();

            foreach (VoxelClass voxel in allVoxels)
            {
                foreach (int neighbourIndex in voxel.parentIndexes)
                {
                    parentIndex.Add(neighbourIndex);
                }
            }

            return parentIndex;
        }

        List<VoxelClass> GetThisVoxelNeighbours (VoxelClass thisVoxel)
        {
            List<VoxelClass> neighbourVoxels = new List<VoxelClass>();

            
            foreach (VoxelClass neighbour in thisVoxel.neighbours)
            {
                neighbourVoxels.Add(neighbour);
            }

            return neighbourVoxels;
        }

        /// <summary>
        /// The Exposure property controls where in the panel a component icon 
        /// will appear. There are seven possible locations (primary to septenary), 
        /// each of which can be combined with the GH_Exposure.obscure flag, which 
        /// ensures the component will only be visible on panel dropdowns.
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// Provides an Icon for every component that will be visible in the User Interface.
        /// Icons need to be 24x24 pixels.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                // You can add image files to your project resources and access them like this:
                //return Resources.IconForThisComponent;
                return null;
            }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("650a5eba-35fd-4c24-b4d2-ee68d4795454"); }
        }
    }
}
