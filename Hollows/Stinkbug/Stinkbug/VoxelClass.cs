﻿using Rhino;
using Rhino.Geometry;
using Rhino.DocObjects;
using Rhino.Collections;

using GH_IO;
using GH_IO.Serialization;
using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;

using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Stinkbug
{
    class VoxelClass
    {
        public static int idCounter;
        public int clusterID;
        public Point3d centroid;

        public List<VoxelClass> neighbours;
        public List<VoxelClass> cluster;
        public List<int> parentIndexes;
        public List<Point3d> testedNeighbourcentroids;
        public bool hasNeighbours;

        public int type;
        //0 inner voxel
        //1 outer voxel
        //2 in contact with tree

        public VoxelClass(Point3d _centroid)
        {
            centroid = new Point3d(_centroid);
            neighbours = new List<VoxelClass>();
            cluster = new List<VoxelClass>();

            type = -1;

            testedNeighbourcentroids = new List<Point3d>();
            parentIndexes = new List<int>();
            hasNeighbours = false;

            clusterID = -1;

            //neighbours.Add(new VoxelClass(new Point3d(3, 4, 5)));
        }
    }
}
